defmodule Problem2 do
  def solver(i, _, total) when (i >= 4_000_000) do
    total
  end

  def solver(i, j, total) do
    IO.puts("#{i}, #{j}")
    total =
      if rem(i, 2) == 0 do
        total + i
      else
        total
      end
    solver(j, i + j, total)
  end
end

IO.puts Problem2.solver(1, 2, 0)