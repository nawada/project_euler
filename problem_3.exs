defmodule Problem3 do
  def solver(n, i, nums) when (n == i) do
    nums ++ [i]
  end

  def solver(n, i, nums) when (rem(n, i) == 0) do
    solver(div(n, i), i + 1, nums ++ [i])
  end
  
  def solver(n, i, nums) do
    solver(n, i + 1, nums)
  end
end

IO.inspect Problem3.solver(600851475143, 2, [])