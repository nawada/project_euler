defmodule Problem1 do
  def solver(n \\ 0) do
    Enum.filter(1..(n - 1), &(rem(&1, 3) == 0 || rem(&1, 5) == 0))
    |> Enum.sum
  end
end

IO.inspect Problem1.solver(1000)